document.body.innerHTML = `<div class="container"></div>`;
const { cardArray } = require("./main.js");
test("This array should only contain objects", () => {
  cardArray.forEach(card => {
    expect(typeof card).toBe("object");
  });
});
test("Object should contain only a title, imageUrl and text key ", () => {
  cardArray.forEach(card => {
    expect(card.title).not.toBe(undefined);
    expect(card.imageUrl).not.toBe(undefined);
    expect(card.content).not.toBe(undefined);
  });
});
test("No object property should be empty", () => {
  cardArray.forEach(card => {
    expect(card.title).not.toBe("");
    expect(card.imageUrl).not.toBe("");
    expect(card.content).not.toBe("");
  });
});
